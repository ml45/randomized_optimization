import mlrose_hiive
import numpy as np
import pandas as pd
import logging
import matplotlib.pyplot as plt
import pickle

if __name__ == "__main__":
#     algos = ["rhc", "sa", "ga", "mimic"]
#     problem = "flipflop"
#
#     combined = pd.DataFrame(columns=[f"{algo}{metric}" for metric in ["iteration", "max_fitness", "fevals", "time"] for algo in algos])
#
#     complexities = [int(i) for i in 2 ** np.arange(2, 8)]
#     for complexity in complexities:
#         print("foo")
#
#
# '''
# go thru every folder for this problem
#
# '''

    # hyperparam_names = {
    #     "rhc": ["current_restart"],
    #     "sa": ["Temperature"],
    #     "ga": ["Population Size", "Mutation Rate"],
    #     "mimic": ["Population Size", "Keep Percent"]
    # }
    #
    # algorithm = "sa"
    # bitstring_length = 256
    # problem_name="flipflop"
    # new_seed = 1337
    # plt.rc('axes', titlesize=14)
    # plt.rc('axes', labelsize=14)
    # plt.rc('xtick', labelsize=14)
    # plt.rc('ytick', labelsize=14)
    # plt.xlabel("iterations")
    # plt.ylabel("fitness")
    # df_run_curves = pickle.load(open("./output/flipflop256-1337/sa__flipflop256-1337__curves_df.p", 'rb'))
    # df_run_curves_separate = []
    # first = True
    # start = 0
    # last_iteration = 0
    # for index, row in df_run_curves.iterrows():
    #     if row["Iteration"] - last_iteration != 1 and not first:
    #         # if row["Iteration"] == 1 and not first:
    #         df_run_curves_separate.append(df_run_curves["Fitness"][start:index])
    #         plt.plot(df_run_curves["Fitness"][start:index].to_list(),
    #                  label=" ".join([str(last_row[hp]) for hp in hyperparam_names[algorithm]]))
    #         start = index
    #     first = False
    #     last_iteration = row["Iteration"]
    #     last_row = row
    #
    # plt.plot(df_run_curves["Fitness"][start:].to_list(),
    #          label=" ".join([str(last_row[hp]) for hp in hyperparam_names[algorithm]]))
    #
    # plt.axhline(y=bitstring_length - 1, linestyle='--', color='k')
    # plt.legend()
    #
    # plt.title(f"{problem_name}{bitstring_length} {algorithm}")
    # plt.ylabel("fitness")
    # # plt.savefig(
    # #     f"./output/{problem_name}{bitstring_length}-{new_seed}/{algorithm}__{problem_name}{bitstring_length}__fitness.png")
    # plt.show()
    # plt.close()

    pickle.load(open(""))
    from sklearn.metrics import precision_recall_curve
    from sklearn.metrics import plot_precision_recall_curve
    import matplotlib.pyplot as plt

    disp = plot_precision_recall_curve(classifier, X_test, y_test)
    disp.ax_.set_title('2-class Precision-Recall curve: '
                       'AP={0:0.2f}'.format(average_precision))