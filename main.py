import mlrose_hiive
import numpy as np
import logging

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.metrics import accuracy_score
from mlrose_hiive import QueensGenerator, MaxKColorGenerator, TSPGenerator
from mlrose_hiive import SARunner, GARunner, NNGSRunner

# switch off the chatter
logging.basicConfig(level=logging.WARNING)


# Load and Split data into training and test sets
data = load_iris()
X_train, X_test, y_train, y_test = train_test_split(
    data.data, data.target, test_size = 0.3, random_state = 123456
)

# Normalize feature data
scaler = MinMaxScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# One hot encode target values
one_hot = OneHotEncoder()
y_train_hot = one_hot.fit_transform(y_train.reshape(-1, 1)).todense()
y_test_hot = one_hot.transform(y_test.reshape(-1, 1)).todense()

grid_search_parameters = {
    'max_iters': [10000],                          # nn params
    'learning_rate': [1e-2, 1e-3, 1e-4, 1e-1],                       # nn params
    'activation': [mlrose_hiive.relu],            # nn params
    'restarts': [1],                             # rhc params
}

nnr = NNGSRunner(
    x_train=X_train_scaled,
    y_train=y_train_hot,
    x_test=X_test_scaled,
    y_test=y_test_hot,
    experiment_name='nn_test_rhc',
    algorithm=mlrose_hiive.algorithms.rhc.random_hill_climb,
    grid_search_parameters=grid_search_parameters,
    iteration_list=[1, 10, 50, 100, 250, 500, 1000],
    hidden_layer_sizes=[[10], [20], [30]],
    bias=True,
    early_stopping=True,
    clip_max=5,
    max_attempts=500,
    n_jobs=-1,
    seed=123456,
    output_directory="./output/"
)


run_stats_df, curves_df, cv_results_df, grid_search_cv = nnr.run()

y_test_pred = grid_search_cv.predict(X_test_scaled)
y_test_accuracy = accuracy_score(y_test_hot, y_test_pred)
print(y_test_accuracy)

y_train_pred = grid_search_cv.predict(X_train_scaled)
y_train_accuracy = accuracy_score(y_train_hot, y_train_pred)
print(y_train_accuracy)

