import mlrose_hiive
import numpy as np
import pandas as pd
import logging
import matplotlib.pyplot as plt
import multiprocessing

def run_flipflop(algorithm, seed, bitstring_length):
    problem = mlrose_hiive.FlipFlopGenerator().generate(seed=seed, size=bitstring_length)
    problem.set_mimic_fast_mode(fast_mode=True)

    problem_name = "flipflop"
    print(f"Start {problem_name}{bitstring_length}  {algorithm}")

    # want max flipflops in general, global optima = bitstring_length-1
    # [1, 0, 1],  [0, 1, 0] = 2
    # [1, 0, 1, 0], [0, 1, 0, 1] = 3

    max_attempts = 100
    max_iters = 4096
    iteration_list = range(0, max_iters, 32)

    pop_size=[100, 200, 300]
    mutation_rate = [0.025, 0.05, 0.01, 0.1, 0.2]
    keep_percent_list = [0.125, 0.25, 0.5, 0.75]
    temperature_list = 2.0 ** np.arange(-3, 10)

    # pop_size=[100]
    # mutation_rate = [0.01]
    # keep_percent_list = [0.5]
    # temperature_list = [8]

    hyperparam_names = {
        "rhc": ["current_restart"],
        "sa": ["Temperature"],
        "ga": ["Population Size", "Mutation Rate"],
        "mimic": ["Population Size", "Keep Percent"]
    }
    new_seed = seed
    if algorithm == "rhc":
        runner = mlrose_hiive.RHCRunner(problem=problem, experiment_name=f"{problem_name}{bitstring_length}-{new_seed}", seed=new_seed,
                                        iteration_list=iteration_list, restart_list=[10],
                                        max_attempts=max_attempts, max_iters=max_iters,
                                        output_directory="./output/")
    elif algorithm == "sa":
        runner = mlrose_hiive.SARunner(problem=problem, experiment_name=f"{problem_name}{bitstring_length}-{new_seed}", seed=new_seed,
                                       iteration_list=iteration_list,
                                       temperature_list=temperature_list,
                                       decay_list=[mlrose_hiive.GeomDecay], max_attempts=max_attempts, max_iters=max_iters,
                                       output_directory="./output/")
    elif algorithm == "ga":
        runner = mlrose_hiive.GARunner(problem=problem, experiment_name=f"{problem_name}{bitstring_length}-{new_seed}", seed=new_seed,
                                       iteration_list=iteration_list,
                                       population_sizes=pop_size, mutation_rates=mutation_rate,
                                       max_attempts=max_attempts, max_iters=max_iters,
                                       output_directory="./output/")
    elif algorithm == "mimic":
        runner = mlrose_hiive.MIMICRunner(problem=problem, experiment_name=f"{problem_name}{bitstring_length}-{new_seed}", seed=new_seed,
                                          iteration_list=iteration_list,
                                          keep_percent_list=keep_percent_list, population_sizes=pop_size,
                                          max_attempts=max_attempts, max_iters=max_iters,
                                          output_directory="./output/", use_fast_mimic=True, fast_mimic=True)
    df_run_stats, df_run_curves = runner.run()

    #plt.figure(figsize=(20,10))
    plt.rcParams.update({'font.size':14})

    df_run_curves_separate = []
    first = True
    start = 0
    last_iteration = 0
    for index, row in df_run_curves.iterrows():
        if row["Iteration"] - last_iteration != 1 and not first:
        #if row["Iteration"] == 1 and not first:
            df_run_curves_separate.append(df_run_curves["Fitness"][start:index])
            plt.plot(df_run_curves["Fitness"][start:index].to_list(), label=" ".join([str(last_row[hp]) for hp in hyperparam_names[algorithm]]))
            start = index
        first = False
        last_iteration = row["Iteration"]
        last_row = row


    plt.plot(df_run_curves["Fitness"][start:].to_list(),
             label=" ".join([str(last_row[hp]) for hp in hyperparam_names[algorithm]]))

    plt.axhline(y=bitstring_length - 1, linestyle='--', color='k')
    plt.legend()
    # for run in df_run_curves_separate:
    #     plt.plot(run.to_list())
    # plt.plot(df_run_curves['Fitness'])
    plt.title(f"{problem_name}{bitstring_length} {algorithm}")
    plt.ylabel("fitness")
    plt.savefig(f"./output/{problem_name}{bitstring_length}-{new_seed}/{algorithm}__{problem_name}{bitstring_length}__fitness.png")
    #plt.show()
    plt.close()
    print(f"End {problem_name}{bitstring_length}  {algorithm}")
    #print(f"Best {algorithm} runs{df_run_curves[df_run_curves['Fitness']==df_run_curves['Fitness'].max()]}")



if __name__ == "__main__":
    logging.basicConfig(level=logging.WARNING)
    seed = 1337
    #bitstring_length = 1024
    #run_flipflop("sa",seed,bitstring_length)
    jobs = []
    #complexity = 2 ** np.arange(2, 9)
    complexity = [256]
    algos = ["rhc", "sa", "ga", "mimic"]
    for bitstring_length in complexity:
        jobs = []
        for algo in algos:
            p = multiprocessing.Process(target=run_flipflop, args=(algo, seed, int(bitstring_length)))
            jobs.append(p)
            p.start()
        multiprocessing.connection.wait(p.sentinel for p in jobs)

    print("Done!")