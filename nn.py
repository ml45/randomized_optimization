import mlrose_hiive
import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder, StandardScaler
from sklearn.metrics import accuracy_score, f1_score

import matplotlib.pyplot as plt
import multiprocessing
import pickle

import time
import os


def preprocess_dodgers():
    dataset1_fname = "dodgers.csv"
    dataset1 = pd.read_csv(dataset1_fname)

    # dataset1.drop(["game_date", "player_name", "batter", "events", "spin_dir", "spin_rate_deprecated", "break_angle_deprecated",
    #                "zone", "des", "stand", "home_team","away_team","type","hit_location"])
    dataset1 = dataset1[["pitch_type", "release_speed", "release_pos_x", "release_pos_z", "description", "p_throws",
                         "balls", "strikes",
                         "pfx_x", "pfx_z", "vx0", "vy0", "vz0", "ax", "ay", "az", "sz_top", "sz_bot",
                         "effective_speed", "release_spin_rate", "release_pos_y", "at_bat_number", "pitch_number",
                         "plate_x", "plate_z"]]

    dataset1 = dataset1.dropna()

    # https://towardsdatascience.com/categorical-encoding-using-label-encoding-and-one-hot-encoder-911ef77fb5bd
    dataset1_to1hot = ["pitch_type", "p_throws"]
    enc = OneHotEncoder()
    enc_df = pd.DataFrame(enc.fit_transform(dataset1[dataset1_to1hot]).toarray())
    dataset1 = dataset1.join(enc_df)
    dataset1 = dataset1.drop(["pitch_type", "p_throws"], axis=1)
    dataset1 = dataset1.dropna()

    dataset1_X = dataset1.drop(["description"], axis=1)
    # dataset1_X = enc.fit_transform(dataset1_X)

    dataset1_y = dataset1["description"]
    dataset1_y = dataset1_y.replace({"ball": 0, "called_strike": 1})

    X_train, X_test, y_train, y_test = train_test_split(dataset1_X, dataset1_y, test_size=0.2, random_state=1337)
    scaler = StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)

    scaler.fit(X_test)
    X_test = scaler.transform(X_test)
    return X_train, X_test, y_train, y_test


def train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(100,), activation='relu', algorithm='random_hill_climb',
             max_iters=10000, bias=True, is_classifier=True, learning_rate=0.0001, early_stopping=True, clip_max=1e10,
             restarts=10, schedule=mlrose_hiive.algorithms.decay.GeomDecay(), pop_size=200, mutation_prob=0.1,
             max_attempts=100, random_state=1, experiment_name=""):
    if not os.path.exists('./output/dodgers/'):
        os.makedirs('./output/dodgers/')
    start_time = time.time()

    nn_model1 = mlrose_hiive.NeuralNetwork(hidden_nodes=hidden_nodes, activation=activation, algorithm=algorithm,
                                           max_iters=max_iters, bias=bias, is_classifier=is_classifier,
                                           learning_rate=learning_rate, early_stopping=early_stopping,
                                           clip_max=clip_max, restarts=restarts, schedule=schedule,
                                           pop_size=pop_size, mutation_prob=mutation_prob, max_attempts=max_attempts,
                                           random_state=random_state, curve=True)
    nn_model1.fit(X_train, y_train)
    # Predict labels for train set and assess accuracy
    y_train_pred = nn_model1.predict(X_train)
    y_train_accuracy = accuracy_score(y_train, y_train_pred)
    #print(y_train_accuracy)

    # Predict labels for test set and assess accuracy
    y_test_pred = nn_model1.predict(X_test)
    y_test_accuracy = accuracy_score(y_test, y_test_pred)
    print(f"{algorithm}:\ttrain:\t{y_train_accuracy}\ttest:\t{y_test_accuracy}\ttime:\t{time.time() - start_time}")
    with open(f"./output/dodgers/{experiment_name}{random_state}-{algorithm}_results.txt", 'a') as results_file:
        print(f"{algorithm}:\ttrain:\t{y_train_accuracy}\ttest:\t{y_test_accuracy}\ttime:\t{time.time() - start_time}", file=results_file)

    #print(nn_model1.fitness_curve)

    if algorithm == "gradient_descent":
        plt.plot(nn_model1.fitness_curve*-1)
    else:
        plt.plot(nn_model1.fitness_curve[:, 0])
    plt.savefig(f"./output/dodgers/{experiment_name}{random_state}-{algorithm}_fitness.png")
    #plt.show()
    plt.close()
    np.savetxt(f"./output/dodgers/{experiment_name}{random_state}-{algorithm}_fitness.csv", nn_model1.fitness_curve, delimiter=',')
    pickle.dump(nn_model1.fitness_curve, open(f"./output/dodgers/{experiment_name}{random_state}-{algorithm}_fitness_curve.p", "wb"))
    return nn_model1

def train_nn_wrapper(X_train, X_test, y_train, y_test, hidden_nodes, learning_rate, algorithm, random_state):
    train_nn(X_train, X_test, y_train, y_test, hidden_nodes=hidden_nodes, learning_rate=learning_rate, algorithm=algorithm, random_state=random_state)


def NNGS(X_train, X_test, y_train, y_test, experiment_name, algorithm, grid_search_parameters, iteration_list,
         hidden_layer_sizes, bias, early_stopping, clip_max, max_attempts, n_jobs=14, seed=1337, output_directory=None):
    nnr = mlrose_hiive.NNGSRunner(x_train=X_train,
                                  y_train=y_train,
                                  x_test=X_test,
                                  y_test=y_test,
                                  experiment_name=experiment_name,
                                  algorithm=algorithm,
                                  grid_search_parameters=grid_search_parameters,
                                  iteration_list=iteration_list,
                                  hidden_layer_sizes=hidden_layer_sizes,
                                  bias=bias,
                                  early_stopping=early_stopping,
                                  clip_max=clip_max,
                                  max_attempts=max_attempts,
                                  n_jobs=n_jobs,
                                  seed=seed,
                                  output_directory=output_directory)
    return nnr.run()

def rhc(X_train, X_test, y_train, y_test, random_state):
    train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(20,), learning_rate=0.1,
             algorithm="random_hill_climb", random_state=random_state, restarts=10)

def sa(X_train, X_test, y_train, y_test, random_state):
    train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(20,), learning_rate=0.001,
             algorithm="simulated_annealing", random_state=random_state, schedule=mlrose_hiive.ExpDecay(0.1))

def ga(X_train, X_test, y_train, y_test, random_state, mutation_rate, pop_size):
    # train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(20,), learning_rate=0.001,
    #          algorithm="genetic_alg", random_state=random_state, mutation_prob=mutation_rate, max_iters=1000, max_attempts=50, experiment_name=f"ga-mut={mutation_rate}")
    train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(20,), learning_rate=0.001,
             algorithm="genetic_alg", random_state=random_state, mutation_prob=mutation_rate, max_iters=1000,
             max_attempts=50, pop_size=pop_size, experiment_name=f"ga-popsize={pop_size}-")

if __name__ == "__main__":
    random_state = 1
    X_train, X_test, y_train, y_test = preprocess_dodgers()

    #rhc(X_train, X_test, y_train, y_test, random_state)
    #sa(X_train, X_test, y_train, y_test, random_state)
    jobs = []
    # for mut_rate in [0.001, 0.05, 0.01, 0.1, 0.2, 0.3]:
    #     p = multiprocessing.Process(target=ga, args=(X_train, X_test, y_train, y_test, random_state, mut_rate))
    #     jobs.append(p)
    #     p.start()
    for pop_size in [10, 50, 100, 200, 400]:
        p = multiprocessing.Process(target=ga, args=(X_train, X_test, y_train, y_test, random_state, 0.01, pop_size))
        jobs.append(p)
        p.start()

    exit()

    algos = ['random_hill_climb',
                    'simulated_annealing', 'genetic_alg',
                    'gradient_descent']
    jobs = []
    for random_state in range(5):
        for algo in algos:
            p = multiprocessing.Process(target=train_nn_wrapper, args=(X_train, X_test, y_train, y_test, (20,), 0.001, algo, random_state))
            #p = multiprocessing.Process(target=train_nn_wrapper, args=(X_train, X_test, y_train, y_test, (20,), 0.01, algo, random_state))

            jobs.append(p)
            p.start()




    # train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(20,), learning_rate=0.001, algorithm="gradient_descent")
    # train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(20,), learning_rate=0.001, algorithm="random_hill_climb")
    # train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(20,), learning_rate=0.001, algorithm="simulated_annealing")
    # train_nn(X_train, X_test, y_train, y_test, hidden_nodes=(20,), learning_rate=0.001, algorithm="genetic_alg")
    #
    # grid_search_parameters = {
    #     #'max_iters': [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024],  # nn params
    #     'max_iters':[1000],
    #     'learning_rate':[0.001],  # nn params
    #     'activation': [mlrose_hiive.relu],  # nn params
    #
    # }
    #
    # iteration_list = [1, 10, 50, 100, 250, 500, 1000]
    # hidden_layer_sizes = [[20], [20, 20], [20, 20, 20]]
    # hidden_layer_sizes = [[20]]

    # gd_gs_params = {}
    # run_stats_df, curves_df, cv_results_df, grid_search_cv = NNGS(X_train, X_test, y_train, y_test, "dodgers-gd",
    #                                                               mlrose_hiive.algorithms.gd.gradient_descent,
    #                                                               {**grid_search_parameters, **gd_gs_params},
    #                                                               iteration_list, hidden_layer_sizes, True, True, 1e10,
    #                                                               200, output_directory="./output/")


    # rhc_gs_params = {
    #     'restarts': [1],  # rhc params
    # }
    # run_stats_df, curves_df, cv_results_df, grid_search_cv = NNGS(X_train, X_test, y_train, y_test, "dodgers-rhc",
    #                                                               mlrose_hiive.algorithms.rhc.random_hill_climb,
    #                                                               {**grid_search_parameters, **rhc_gs_params},
    #                                                               iteration_list, hidden_layer_sizes, True, True, 5,
    #                                                               500, output_directory="./output/")

    # sa_gs_params = {}
    # run_stats_df, curves_df, cv_results_df, grid_search_cv = NNGS(X_train, X_test, y_train, y_test, "dodgers-sa",
    #                                                               mlrose_hiive.algorithms.sa.simulated_annealing,
    #                                                               {**grid_search_parameters, **sa_gs_params},
    #                                                               iteration_list, hidden_layer_sizes, True, True, 1e10,
    #                                                               500, output_directory="./output/")
    # ga_gs_params = {}
    # run_stats_df, curves_df, cv_results_df, grid_search_cv = NNGS(X_train, X_test, y_train, y_test, "dodgers-ga",
    #                                                               mlrose_hiive.algorithms.ga.genetic_alg,
    #                                                               {**grid_search_parameters, **ga_gs_params},
    #                                                               iteration_list, hidden_layer_sizes, True, True, 1e10,
    #                                                               500, output_directory="./output/")
    # y_test_pred = grid_search_cv.predict(X_test)
    # y_test_accuracy = f1_score(y_test, y_test_pred, average="macro")
    # print(y_test_accuracy)
    # y_train_pred = grid_search_cv.predict(X_train)
    # y_train_accuracy = f1_score(y_train, y_train_pred, average="macro")
    # print(y_train_accuracy)