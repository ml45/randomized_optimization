# Assignment 2 - Randomized Optimization

## About
Timothy Isonio

tisonio3@gatech.edu

CS 7641 - Machine Learning - Spring 2021
Code and extras located at:
https://gitlab.com/ml45/randomized_optimization

This is a Markdown document with a .txt extension.

## Environment
In the code repo is the environment.yml needed to replicate the Conda environment I used
for this project. If that does not work, in essence creating an environment with these:

    python>=3.8 numpy scipy scikit-learn matplotlib pandas

should work too. And of course:

    pip install mlrose_hiive

I did this project on a Windows 10 PC running an AMD 8-core/16-thread processor,
so there might be some Windows specific stuff in there. Sometimes I hard-coded 14 jobs
so that I had a core to actually use myself while it ran.

## Running the code
Each of the problems has its own file. Running it alone will run it with default params,
and some have command line options.

* flipflop
* nqueens
* onemax
* nn

These problems will output to folders in the output directory. Each will output many pickled
dataframes, csvs, and png figures. I tried to keep them organized. I also included the results
files in the github repo.

In addition, I manually took lines from each of these CSVs into Excel to produce several graphs.
These xlsx files are also included.

The code will have sections commented out that may have been run before. I know it's messy. I know
I should do a Jupyter Notebook instead, but I really like having the full PyCharm IDE, and
the notebook interface is not good IMO.

## References
* mlrose_hiive - invaluable help. Maybe too much help?
* mlrose example Juypter Notebook
* Baseball Statcast dataset
* The articles also cited in the report