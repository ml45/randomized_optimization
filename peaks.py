import mlrose_hiive
import numpy as np
import pandas as pd
import logging
import matplotlib.pyplot as plt
import multiprocessing

def run_nqueens(algorithm, seed, n):
    problem = mlrose_hiive.ContinuousPeaksGenerator().generate(seed=seed, size=n, t_pct=0.1)
    problem_name = "peaks"
    print(f"Start {problem_name}{n}  {algorithm}")

    # want max flipflops in general, global optima = bitstring_length-1
    # [1, 0, 1],  [0, 1, 0] = 2
    # [1, 0, 1, 0], [0, 1, 0, 1] = 3

    max_attempts = 200

    hyperparam_names = {
        "rhc": ["current_restart"],
        "sa": ["Temperature"],
        "ga": ["Population Size", "Mutation Rate"],
        "mimic": ["Population Size", "Keep Percent"]
    }

    if algorithm == "rhc":
        runner = mlrose_hiive.RHCRunner(problem=problem, experiment_name=f"{problem_name}{n}", seed=seed,
                                        iteration_list=2 ** np.arange(11), restart_list=[10],
                                        max_attempts=max_attempts, max_iters=max_attempts*20,
                                        output_directory="./output/")
    elif algorithm == "sa":
        runner = mlrose_hiive.SARunner(problem=problem, experiment_name=f"{problem_name}{n}", seed=seed,
                                       iteration_list=2 ** np.arange(11),
                                       temperature_list=2.0 ** np.arange(-3, 10),
                                       decay_list=[mlrose_hiive.GeomDecay], max_attempts=max_attempts, max_iters=max_attempts*20,
                                       output_directory="./output/")
    elif algorithm == "ga":
        runner = mlrose_hiive.GARunner(problem=problem, experiment_name=f"{problem_name}{n}", seed=seed,
                                       iteration_list=2 ** np.arange(11),
                                       population_sizes=[150, 200, 300], mutation_rates=[0.025, 0.05, 0.1, 0.2],
                                       max_attempts=max_attempts, max_iters=max_attempts*20,
                                       output_directory="./output/")
    elif algorithm == "mimic":
        runner = mlrose_hiive.MIMICRunner(problem=problem, experiment_name=f"{problem_name}{n}", seed=seed,
                                          iteration_list=2 ** np.arange(11),
                                          keep_percent_list=[0.25, 0.5, 0.75], population_sizes=[200],
                                          max_attempts=max_attempts, max_iters=max_attempts*20,
                                          output_directory="./output/", use_fast_mimic=True)
    df_run_stats, df_run_curves = runner.run()

    plt.figure(figsize=(20,10))

    df_run_curves_separate = []
    first = True
    start = 0
    last_iteration = 0
    for index, row in df_run_curves.iterrows():
        if row["Iteration"] - last_iteration != 1 and not first:
        #if row["Iteration"] == 1 and not first:
            df_run_curves_separate.append(df_run_curves["Fitness"][start:index])
            plt.plot([i for i in df_run_curves["Fitness"][start:index].to_list()], label=" ".join([str(last_row[hp]) for hp in hyperparam_names[algorithm]]))
            start = index
        first = False
        last_iteration = row["Iteration"]
        last_row = row

    plt.plot([i for i in df_run_curves["Fitness"][start:].to_list()],
             label=" ".join([str(last_row[hp]) for hp in hyperparam_names[algorithm]]))

    #plt.axhline(y=n - 1, linestyle='--', color='k')
    plt.legend()
    # for run in df_run_curves_separate:
    #     plt.plot(run.to_list())
    # plt.plot(df_run_curves['Fitness'])
    plt.title(f"{problem_name}{n} {algorithm}")
    plt.ylabel("fitness")
    plt.savefig(f"./output/{problem_name}{n}/{algorithm}__{problem_name}{n}__fitness.png")
    #plt.show()
    plt.close()
    print(f"End {problem_name}{n}  {algorithm}")
    #print(f"Best {algorithm} runs{df_run_curves[df_run_curves['Fitness']==df_run_curves['Fitness'].max()]}")



if __name__ == "__main__":
    logging.basicConfig(level=logging.WARNING)
    seed = 1337
    n = 1024
    #run_flipflop("sa",seed,bitstring_length)
    jobs = []
    algos = ["rhc", "sa", "ga", "mimic"]
    for algo in algos:
        p = multiprocessing.Process(target=run_nqueens, args=(algo, seed, n))
        jobs.append(p)
        p.start()
    #
    # run_flipflop("rhc", seed, bitstring_length)
    # run_flipflop("sa", seed, bitstring_length)
    # run_flipflop("ga", seed, bitstring_length)
    # run_flipflop("mimic", seed, bitstring_length)

    print("Done!")